package rework;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Check informations from entered user
 *
 * @author Admin
 */
public class Validate {

    public boolean isWords(String name) {
        //Regex mean: name include many character and space
        String regex = "[a-z\\s]+";
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(name);
        if (matcher.matches() && !name.isEmpty()) {
            return true;
        }
        return false;
    }

    public boolean isId(String id) {
        //Regex mean: name include many character and integer number
        String regex = "[a-z0-9]+";
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(id);
        if (matcher.matches() && !id.isEmpty()) {
            return true;
        }
        return false;
    }

    public boolean isCourseName(String courseName) {
        //Regex mean: coursce name include java or c/c++ or .net
        String regex = "^java|c/c\\+\\+|.net$";
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(courseName);
        if (matcher.matches() && !courseName.isEmpty()) {
            return true;
        }
        return false;
    }

    public boolean isDuplicate(ArrayList<Student> listStudents,
            Student checkedStudent) {
        //Run from first student to satisfy condition or to last student
        for (Student student : listStudents) {
            //Check id, course name and semester equals of two student
            if (student.getId().compareToIgnoreCase(checkedStudent.getId()) == 0
                    && student.getCourseName().compareToIgnoreCase(checkedStudent.getCourseName()) == 0
                    && student.getSemester() == checkedStudent.getSemester()) {
                return true;
            }
        }
        return false;
    }

    public boolean isSameIdAndDifName(ArrayList<Student> listStudents,
            String id, String name) {
        //Run from first student to satisfy condition or to last student
        for (Student student : listStudents) {
            //Check same id and diffirent name
            if (student.getId().compareToIgnoreCase(id) == 0
                    && student.getName().compareToIgnoreCase(name) != 0) {
                return true;
            }
        }
        return false;
    }

    public boolean isYesOrNo(String input) {
        //Regex mean: input include y or n
        String regex = "[y|n]";
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(input);
        if (matcher.matches()) {
            return true;
        }
        return false;
    }

    public boolean isupdateOrDelete(String input) {
        //Regex mean: input include u or d
        String regex = "[u|d]";
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(input);
        if (matcher.matches()) {
            return true;
        }
        return false;
    }

    public boolean sameStudentWithCourse(Student student1, Student student2) {
        //Check id, course equals of two diffirence students
        if (!student1.equals(student2)
                && student1.getId().compareToIgnoreCase(student2.getId()) == 0
                && student1.getCourseName().compareToIgnoreCase(student2.getCourseName()) == 0) {
            return true;
        }
        return false;
    }

    public boolean isSameIDWithList(ArrayList<Student> listStudents, String id) {
        //Run from first student to satisfy condition or to last student
        for (Student student : listStudents) {
            //Check same id and diffirent name
            if (student.getId().compareToIgnoreCase(id) == 0) {
                return true;
            }
        }
        return false;
    }

    public boolean isSameID(String id, String id0) {
        if (id.compareToIgnoreCase(id0) == 0) {
            return true;
        }
        return false;
    }

    public boolean isDuplicateAll(ArrayList<Student> listStudents,
            Student checkedStudent, Student studentByIndex) {
        String newID = checkedStudent.getId();
        String newName = checkedStudent.getName();
        String newCourseName = checkedStudent.getCourseName();
        int newSemester = checkedStudent.getSemester();
        //Check id, name, course name and semester equals of two student
        if (studentByIndex.getId().compareToIgnoreCase(newID) == 0
                && studentByIndex.getName().compareToIgnoreCase(newName) == 0
                && studentByIndex.getCourseName().compareToIgnoreCase(newCourseName) == 0
                && studentByIndex.getSemester() == newSemester) {
            return true;
        }
        //Run from first student to satisfy condition or to last student
        for (Student student : listStudents) {
            //Check id, course name and semester equals of two student
            if (student.getId().compareToIgnoreCase(newID) == 0
                    && student.getCourseName().compareToIgnoreCase(newCourseName) == 0
                    && student.getSemester() == newSemester
                    && !studentByIndex.equals(student)) {
                return true;
            }
        }
        return false;
    }
}
