/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rework;

import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class Main {

    public static void main(String[] args) {
        ArrayList<Student> listStudents = new ArrayList<>();
        StudentManagement manage = new StudentManagement();
        int choice;
        do {
            //Step 1: Display a menu
            manage.displayMenu();
            //Step 2: User choice option
            choice = manage.choiceOption();
            switch (choice) {
                case 1:
                    //Step 3: Option 1: Create student
                    manage.createStudent(listStudents);
                    break;
                case 2:
                    //Step 4: Option 2: Find and Sort student
                    manage.findAndSortStudent(listStudents);
                    break;
                case 3:
                    //Step 5: Option 3: Update/Delete student
                    manage.updateOrDeleteStudent(listStudents);
                    break;
                case 4:
                    //Step 6: Option 4: Report student
                    manage.reportStudent(listStudents);
                    break;
                case 5:
                    //Step 7: Option 5: Exit
                    manage.exit();
                    break;
            }
        } while (choice != 5);
    }
}
















//        listStudents.add(new Student("1", "Dinh Tien", 2, "Java"));
//        listStudents.add(new Student("1", "Dinh Tien", 2, ".Net"));
//        listStudents.add(new Student("1", "Dinh Tien", 3, "Java"));
//        listStudents.add(new Student("1", "Dinh Tien", 3, ".Net"));
//        listStudents.add(new Student("2", "Nguyen A", 2, "C/C++"));
//        listStudents.add(new Student("2", "Nguyen A", 3, "C/C++"));
//        listStudents.add(new Student("3", "Tran Tien", 2, "Java"));
//        listStudents.add(new Student("3", "Tran Tien", 2, ".Net"));
//        listStudents.add(new Student("3", "Tran Tien", 3, ".Net"));
//        listStudents.add(new Student("3", "Tran Tien", 3, ".Java"));