package rework;

import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * User can enter information from keyboard
 * Tien dep trai
 * @author Admin
 */
public class Input {

    public Scanner sc = new Scanner(System.in);

    public int option(String message, int first, int last) {
        int input = 0;
        //Run again until number is integer number
        //and integer number is in range between 1 and 6 so stop
        do {
            try {
                System.out.println(message);
                input = Integer.parseInt(sc.nextLine());
                if (input >= first && input <= last) {
                    break;
                }
                if (input < first || input > last) {
                    System.out.println("Must from " + first + " to"
                            + last + ". Pls, enter again!");
                }
            } catch (Exception e) {
                System.out.println("Must integer number. Pls, enter again!");
            }
        } while (true);
        return input;
    }

    public int integerNumber(String message) {
        int input = 0;
        //Run again until number is integer number so stop
        do {
            try {
                System.out.println(message);
                input = Integer.parseInt(sc.nextLine());
                break;
            } catch (Exception e) {
                System.out.println("Must integer number. Pls, enter again!");
            }
        } while (true);
        return input;
    }

    public float floatNumber(String message) {
        float input = 0;
        //Run again until number is float number
        //and float number greater than 0 so stop
        do {
            try {
                System.out.println(message);
                input = Float.parseFloat(sc.nextLine());
                if (input > 0) {
                    break;
                } else {
                    System.out.println("Total area must be greater than 0."
                            + "Pls, enter again!");
                }
            } catch (Exception e) {
                System.out.println("Must number. Pls, enter again!");
            }
        } while (true);
        return input;
    }

    public String stringWords(String message) {
        Validate check = new Validate();
        String input = "";
        //Run again until check valid words is true so stop
        do {
            System.out.println(message);
            input = sc.nextLine();
            if (check.isWords(input)) {
                break;
            } else {
                System.out.println("Must words. Pls, enter again!");
            }
        } while (true);
        return input;
    }

    public String stringCode(String message) {
        Validate check = new Validate();
        String input = "";
        //Run again until check valid code is true so stop
        do {
            System.out.println(message);
            input = sc.nextLine();
            if (check.isId(input)) {
                break;
            } else {
                System.out.println("Must code. Pls, enter again!");
            }
        } while (true);
        return input;
    }

    public String stringSource(String message) {
        Validate check = new Validate();
        String input = "";
        //Run again until check valid id is true so stop
        do {
            System.out.println(message);
            input = sc.nextLine();
            if (check.isCourseName(input)) {
                break;
            } else {
                System.out.println("Must source name. Pls, enter again!");
            }
        } while (true);
        return input;
    }

    public String yesOrNo(String message) {
        Validate check = new Validate();
        String input = "";
        //Run again until check  is y or n so break loop
        do {
            System.out.println(message);
            input = sc.nextLine();
            if (check.isYesOrNo(input)) {
                return input;
            } else {
                System.out.println("Must Y or N. Pls, enter again!");
            }
        } while (true);
    }
    
    public String updateOrDelete(String message) {
        Validate check = new Validate();
        String input = "";
        //Run again until check  is u or d so break loop
        do {
            System.out.println(message);
            input = sc.nextLine();
            if (check.isupdateOrDelete(input)) {
                return input;
            } else {
                System.out.println("Must U or D. Pls, enter again!");
            }
        } while (true);
    }
}
